//
//  MenuView.m
//  Reveal Art
//
//  Created by Sonya Hochkammer on 11/23/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "MenuView.h"

@implementation MenuView

UIImageView *actionButton;
UIImageView *newImageButton;
UIImageView *saveButton;

UIImageView *highlightView;
UIImageView *currentView;

Boolean menuOpen;

@synthesize delegate;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setMultipleTouchEnabled:NO];
        [self setupControls];
    }
    return self;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        [self setupControls];
    }
    return self;
}

- (void)setupControls{
    
    menuOpen = NO;
    
    highlightView = [UIImageView new];
    [highlightView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1]];
    [highlightView.layer setCornerRadius:30.0];
    [highlightView setFrame:CGRectMake(0,0,60,60)];
    [highlightView setClipsToBounds:TRUE];
    [highlightView setHidden:TRUE];
    [self addSubview:highlightView];
    
    actionButton = [UIImageView new];
    /*[actionButton addTarget:self
                     action:@selector(revealMenu)
           forControlEvents:UIControlEventTouchDown];
    
    [actionButton addTarget:self
                     action:@selector(hideMenu)
           forControlEvents:UIControlEventTouchUpInside];*/
    
    [actionButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1]];
    //[actionButton setShowsTouchWhenHighlighted:YES];
    [actionButton setImage:[UIImage imageNamed:@"actions.png"]];
    [actionButton.layer setCornerRadius:20.0];
    [actionButton setClipsToBounds:TRUE];
    [self addSubview:actionButton];
    
    newImageButton = [UIImageView new];
    /*[newImageButton addTarget:self
                       action:@selector(newImage)
             forControlEvents:UIControlEventTouchDragOutside];*/
    [newImageButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1]];
    [newImageButton setImage:[UIImage imageNamed:@"newImage.png"]];
    [newImageButton.layer setCornerRadius:20.0];
    [newImageButton setClipsToBounds:TRUE];
    [newImageButton setHidden:TRUE];
    [self addSubview:newImageButton];
    
    saveButton = [UIImageView new];
    /*[saveButton addTarget:self
     action:@selector(revealActionsMenu)
     forControlEvents:UIControlEventTouchCancel];*/
    [saveButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1]];
    [saveButton setImage:[UIImage imageNamed:@"save.png"]];
    [saveButton.layer setCornerRadius:20.0];
    [saveButton setClipsToBounds:TRUE];
    [saveButton setHidden:TRUE];
    [self addSubview:saveButton];
    
    [highlightView setCenter:actionButton.center];

}

-(void)layoutSubviews{
    [newImageButton setFrame:CGRectMake(self.bounds.size.width -40, self.bounds.size.height -40, 40, 40)];
    [actionButton setFrame:CGRectMake(self.bounds.size.width -40, self.bounds.size.height -40, 40, 40)];
    [saveButton setFrame:CGRectMake(self.bounds.size.width -40, self.bounds.size.height -40, 40, 40)];
}

-(void) revealMenu{

    menuOpen = YES;
    
    [saveButton setHidden:FALSE];
    [newImageButton setHidden:FALSE];
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [saveButton setFrame:CGRectMake(self.bounds.size.width -120, self.bounds.size.height -100, 40, 40)];
                         [newImageButton setFrame:CGRectMake(self.bounds.size.width -45, self.bounds.size.height -130, 40, 40)];
                     }
                     completion:^(BOOL finished){
                         //
                     }];
}

-(void) hideMenu{
    
    menuOpen= NO;
    currentView = nil;
    
    [saveButton setHidden:FALSE];
    [newImageButton setHidden:FALSE];
    [highlightView setHidden:TRUE];
    
    [saveButton setAlpha:1];
    [newImageButton setAlpha:1];
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [saveButton setFrame:CGRectMake(self.bounds.size.width -40, self.bounds.size.height -40, 40, 40)];
                         [newImageButton setFrame:CGRectMake(self.bounds.size.width -40, self.bounds.size.height -40, 40, 40)];
                         
                         [saveButton setAlpha:0];
                         [newImageButton setAlpha:0];
                         
                     }
                     completion:^(BOOL finished){
                         [saveButton setHidden:TRUE];
                         [newImageButton setHidden:TRUE];
                         
                         [saveButton setAlpha:1];
                         [newImageButton setAlpha:1];
                     }];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint point = [[touches anyObject] locationInView:self];

    UIView  *previousView = currentView;
    
    if(CGRectContainsPoint(newImageButton.frame, point)){
        currentView = newImageButton;
    }else if(CGRectContainsPoint(saveButton.frame, point)){
        currentView = saveButton;
    }else{
        currentView = nil;
    }
    
    if(currentView && (currentView !=previousView)){
        [highlightView setCenter:currentView.center];
        [highlightView setHidden:false];
        [highlightView setAlpha:0];
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [highlightView setAlpha:1];
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
    }else if(!currentView){
        [highlightView setHidden:true];
    }
    
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self revealMenu];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if(currentView == newImageButton){
        [delegate newImage];
    }else if(currentView == saveButton){
        [delegate saveImage];
    }
    
    [self hideMenu];
}

- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    Boolean pointInside = NO;
    
    if(CGRectContainsPoint(actionButton.frame,point)){
        pointInside=YES;
    }else if(menuOpen) {
        pointInside=NO;
        [self hideMenu];
    }
    
    return pointInside;
}

@end
