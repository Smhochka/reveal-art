//
//  ScratchOffView.m
//  Reveal Art
//
//  Created by Sonya Hochkammer on 11/20/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "ScratchOffView.h"

#define LINE_WIDTH 3.0
#define LINE_CAP_STYLE kCGLineCapRound
#define GRAY_BACKGROUND_COLOR [UIColor colorWithHue:(165.0/255.0) saturation:(28.0/255.0) brightness:(50/255.0) alpha:(1)]

@implementation ScratchOffView

UIBezierPath *path;
UIImage *incrementalImage;
CGPoint pts[5]; // four points of a Bezier segment and the first control point of the next segment
uint ctr;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:LINE_WIDTH];
        [path setLineCapStyle:LINE_CAP_STYLE];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if (!incrementalImage) // first time; paint background
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [GRAY_BACKGROUND_COLOR setFill];
        [rectpath fill];
    }
    
    [incrementalImage drawInRect:rect];
    [path strokeWithBlendMode:kCGBlendModeClear alpha:1];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        
        [path moveToPoint:pts[0]];
        [path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        
        [self setNeedsDisplay];
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    
    if([path isEmpty]){
        path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(p.x-(LINE_WIDTH/2), p.y-(LINE_WIDTH/2), LINE_WIDTH, LINE_WIDTH)];
        [path setLineWidth:LINE_WIDTH];
        [path setLineCapStyle:LINE_CAP_STYLE];
    }
    
    [self drawBitmap];
    [self setNeedsDisplay];
    [path removeAllPoints];
    ctr = 0;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    
    if (!incrementalImage) // first time;
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [GRAY_BACKGROUND_COLOR setFill];
        [rectpath fill];
    }
    
    [incrementalImage drawAtPoint:CGPointZero];
    [path strokeWithBlendMode:kCGBlendModeClear alpha:1];
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

-(void) reset{
    incrementalImage = nil;
    
    [path removeAllPoints];
    [self drawBitmap];
    [self setNeedsDisplay];
}

@end
