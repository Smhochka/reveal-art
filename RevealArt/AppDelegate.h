//
//  AppDelegate.h
//  Reveal Art
//
//  Created by Sonya Hochkammer on 11/20/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

