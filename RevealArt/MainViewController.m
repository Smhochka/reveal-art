//
//  ViewController.m
//  Reveal Art
//
//  Created by Sonya Hochkammer on 11/20/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "MainViewController.h"
#import "ScratchOffView.h"
#import "MenuView.h"
#import <QuartzCore/QuartzCore.h>

@interface MainViewController ()

@end

@implementation MainViewController

UIImageView *backgroundView;
MenuView *menuView;
ScratchOffView *view;

UISwipeGestureRecognizer *swipeRecognizer;

static NSArray *backgroundImages;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    backgroundView = [UIImageView new];
    [backgroundView setContentMode:UIViewContentModeScaleToFill];
    [backgroundView.layer setCornerRadius:20.0];
    [backgroundView setClipsToBounds:TRUE];
    [self setBackgroundImage];
    [self.view addSubview:backgroundView];
        
    view = [ScratchOffView new];
    [view setBackgroundColor:[UIColor clearColor]];
    [view.layer setCornerRadius:20.0];
    [view setClipsToBounds:TRUE];
    [self.view addSubview:view];
    
    menuView = [MenuView new];
    [menuView.layer setCornerRadius:20.0];
    [menuView setClipsToBounds:TRUE];
    [self.view addSubview:menuView];
    [menuView setDelegate:self];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    [self layoutSubviews];
    
}

-(void) layoutSubviews{
    [view setFrame:self.view.bounds];
    [backgroundView setFrame:self.view.bounds];
    [menuView setFrame:self.view.bounds];

}

-(void) setBackgroundImage{
    if(!backgroundImages){
         backgroundImages = @[@"pink.png",@"blue.png",@"red",@"rainbow.png"];
    }
    
    NSInteger rand = arc4random() % [backgroundImages count];
    [backgroundView setImage:[UIImage imageNamed:backgroundImages[rand]]];
    
}

-(void) newImage{
    [self setBackgroundImage];
    [view reset];
}

-(void) saveImage{
    
    
    [menuView setHidden:TRUE];
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    [menuView setHidden:false];
    
    UIView *flashView = [UIView new];
    [flashView setFrame:self.view.frame];
    [self.view addSubview:flashView];
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [flashView setBackgroundColor:[UIColor colorWithWhite:1 alpha:.3]];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                     }];
    

}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if ( event.subtype == UIEventSubtypeMotionShake )
    {
        [self newImage];
    }
    
    if ( [super respondsToSelector:@selector(motionEnded:withEvent:)] )
        [super motionEnded:motion withEvent:event];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
