//
//  MenuView.h
//  ScratchOff
//
//  Created by Sonya Hochkammer on 11/23/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuActionDelegate <NSObject>

@required
- (void)newImage;
- (void)saveImage;

@end

@interface MenuView : UIView
{
id<MenuActionDelegate> delegate;
}

@property (nonatomic) id delegate;

@end
